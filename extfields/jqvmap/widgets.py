# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from django.forms.widgets import Widget
from django.template import Context, loader
from django.db.models.fields import NOT_PROVIDED

import  settings
from wioframework import wiotext


class WIOJQVMapWidget(Widget):
    class Media:
        js = [
            settings.MEDIA_URL + 'jqvmap/jquery.vmap.js',
            settings.MEDIA_URL + 'jqvmap/maps/jquery.vmap.world.js',
            settings.MEDIA_URL + "js/_extfields-jqvmap.dbg.js"
        ]
    def __init__(self, attrs={}, maxlength=None, default=None,
                 choices=None, required=False, placeholder="", fieldtype="text"):
        self.attrs = attrs
        self.choices = choices
        self.default = default if default is not NOT_PROVIDED else None
        self.required = required
        self.placeholder = placeholder
        self.fieldtype = fieldtype
        self.request = None

    def set_request(self, r):
        self.request = r
        self.request.with_jqv_widget=True

    def render(self, name, value, attrs=None):
        if value is None:
            value = self.default
        widgettemplate = loader.get_template('extfields/jqvmap.html')
        html_content = widgettemplate.render(Context({
            'include_widget': (self.request.with_jqv_widget),
            'name': name,
            'value': ("" if value is None else value),
            'choices': self.choices,
            'required': self.required,
        }))
        self.request.with_jqv_widget=False
        return html_content

    def value_from_datadict(self, data, files, name):
        text = data.get(name,None)
        return text
